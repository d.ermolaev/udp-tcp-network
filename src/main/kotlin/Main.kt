import com.google.common.primitives.Shorts
import network.Channel
import network.ChannelUDP
import network.Network
import network.messages.*
import java.util.*


fun main(args: Array<String>) {


    println("Program arguments: ${args.joinToString()}")

    val CONNECTION_PORT_DEFAULT_PARSES = 10100
    val CONNECTION_PORT = 10101


    val acceptorDefaultParserUDP = ChannelUDP.makeConnectionAcceptor(
        CONNECTION_PORT_DEFAULT_PARSES,
        Message::DEFAULT_PARSER,
        object : MessageProcessor() {
            override fun process(message: Message) {

                if (message is ObjectMessage) {
                    println("OBJECT : " + message.`object`)
                }

                // simple back same message to initiator
                if (message.isRequest) {
                    println("send back: $message")
                    message.setResponseId(message.requestId)
                    channel.send(message)
                }
            }
        },
        Channel.MODE.ALL
    )
    acceptorDefaultParserUDP.start();
    println("Acceptor UTP with [DEFAULT_PARSER] is start on port $CONNECTION_PORT_DEFAULT_PARSES")

    val networkDefaultParser = Network(
        CONNECTION_PORT_DEFAULT_PARSES,
        Message::DEFAULT_PARSER,
        object : MessageProcessor() {
            override fun process(message: Message) {
                // simple back same message to initiator

                println("RECEIVE: $message")

                // simple back same message to initiator
                if (message.isRequest) {
                    println("send back: $message")
                    message.setResponseId(message.requestId)
                    channel.send(message)
                }

            }
        },
        Channel.MODE.ALL
    )
    networkDefaultParser.makeAcceptor();
    networkDefaultParser.start()
    println("Acceptor TCP with [DEFAULT_PARSER] is start on port $CONNECTION_PORT_DEFAULT_PARSES")

    val parser: (ByteArray, ByteArray) -> Message = label@{ head, body ->
        val type = Shorts.fromByteArray(Arrays.copyOfRange(head, 0, Message.TYPE_LENGTH)).toInt()
        val flags: Byte = head[Message.TYPE_LENGTH]

        when (type) {
            // Parse EXAMPLE_1_TYPE message
            Message.EXAMPLE_1_TYPE ->
                return@label if (flags > 0)
                    Message_Example_1(head, body)
                else Message_Example_1(body)

            else -> return@label Message.DEFAULT_PARSER(head, body)
        }
    }

    // as server
    val acceptorUDP = ChannelUDP.makeConnectionAcceptor(
        CONNECTION_PORT, parser,
        object : MessageProcessor() {
            override fun process(message: Message) {
                // simple back same message to initiator

                val messageProcessed = when (message) {
                    is Message_Example_1 -> {
                        Message_Example_1(
                            message.par1 + 100,
                            message.par2 + " :processed"
                        )
                    }

                    is StringMessage -> {
                        val resp = StringMessage(message.message + " :processed")
                        if (message.isRequest) {
                            resp.setResponseId(message.requestId)
                        }
                        resp
                    }

                    else -> message
                }

                println("send back: $messageProcessed")
                channel.send(messageProcessed)
            }
        },
        Channel.MODE.ALL
    )
    acceptorUDP.start();
    println("Acceptor UTP is start on port $CONNECTION_PORT")


    val network = Network(
        CONNECTION_PORT,
        parser,
        object : MessageProcessor() {
            override fun process(message: Message) {
                // simple back same message to initiator

                val messageProcessed = when (message) {
                    is Message_Example_1 -> {
                        Message_Example_1(message.par1 + 100, message.par2 + " :processed")
                    }

                    is StringMessage -> {
                        val resp = StringMessage(message.message + " :RESPONSE")
                        resp.setResponseId(message.requestId)
                        resp
                    }

                    else -> message
                }

                println("send back: $messageProcessed")
                channel.send(messageProcessed)
            }
        },
        Channel.MODE.ALL
    )
    network.makeAcceptor();
    network.start()
    println("Acceptor TCP is start on port $CONNECTION_PORT")


}