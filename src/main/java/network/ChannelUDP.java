package network;


import network.messages.Message;
import network.messages.MessageProcessor;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;


public class ChannelUDP extends Channel<DatagramSocket> {

    private ChannelUDP(int localPort,
                       MODE mode, BiFunction<byte[], byte[], Message> parser, MessageProcessor processor) {
        super(localPort, parser, processor, mode);

    }

    private ChannelUDP(int localPort, InetAddress address, int remotePort,
                       MODE mode, BiFunction<byte[], byte[], Message> parser, MessageProcessor processor) {
        super(localPort, address, remotePort, parser, processor, mode);

    }

    @Override
    public void setRequestsQueue(ConcurrentHashMap requestsQueue) {
        this.requestsQueue = requestsQueue;
    }

    @Override
    public void setReceiver(Receiver receiver) {
        this.receiver = (ReceiverUDP) receiver;
    }

    @Override
    public void setSender(Sender sender) {
        this.sender = (SenderUDP) sender;
    }

    public void start() {

        try {
            if (localPort != 0 )
                socket = new DatagramSocket(localPort);
            else
                socket = new DatagramSocket();


        } catch (SocketException e) {
            System.err.println(this + " " + e);
            return;
        }

        if (mode == Channel.MODE.ALL) {
            requestsQueue = new ConcurrentHashMap<Integer, BlockingQueue<Message>>(requestQueueInitSize, 1);
        }

        if (mode != Channel.MODE.ONLY_SEND) {
            receiver = new ReceiverUDP(this, socket);
            receiver.start();
        }

        if (mode != Channel.MODE.ONLY_RECEIVE) {
            sender = new SenderUDP(this, socket);
            sender.start();
        }

    }

    @Override
    public Message parseMessage(byte[] head, byte[] body) {
        return parser.apply(head, body);
    }

    @Override
    public void processMessage(Message message) {
        processor.process(message);
    }


    public static ChannelUDP makeConnectionAcceptor(int localPort,
                                                    BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, MODE mode) {
        return new ChannelUDP(localPort, mode, parser, processor);
    }

    public static ChannelUDP makeConnectionInitiator(InetAddress address, int remotePort,
                                                     BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, MODE mode) {
        return new ChannelUDP(0, address, remotePort, mode, parser, processor);
    }

    /**
     * Make and start Initiator in Only Send mode
     *
     * @param address
     * @param remotePort
     * @return
     */
    public static ChannelUDP startSender(InetAddress address, int remotePort) {
        ChannelUDP channel = new ChannelUDP(0, address, remotePort, MODE.ONLY_SEND, null, null);
        channel.start();
        return channel;
    }

    public void close() {
        socket.close();
    }

    @Override
    public String toString() {
        return "UDP " + super.toString();
    }

}
