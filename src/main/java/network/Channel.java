package network;

import network.messages.Message;
import network.messages.MessageProcessor;
import org.json.JSONObject;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;


abstract public class Channel<SockT> implements AutoCloseable {

    public enum MODE {
        ALL,
        ONLY_RECEIVE,
        ONLY_SEND
    }

    public enum STATUS {
        OPEN,
        BAN
    }

    SockT socket;
    BiFunction<byte[], byte[], Message> parser;
    MessageProcessor processor;

    protected InetAddress address;

    protected int localPort;
    protected int remotePort;
    MODE mode;

    public Map<Integer, BlockingQueue<Message>> requestsQueue;
    int requestQueueInitSize = 32;

    protected Receiver receiver;
    protected Sender sender;
    public STATUS status;

    /**
     * Make connection Acceptor
     *
     * @param localPort
     * @param mode
     */
    public Channel(int localPort, BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, MODE mode) {
        this.localPort = localPort;

        this.mode = mode;

        if (mode != MODE.ONLY_SEND) {
            this.parser = parser;

            if (processor != null) {
                processor.setChannel(this);
                this.processor = processor;
            }
        }

        if (mode == MODE.ALL) {
            requestsQueue = new ConcurrentHashMap<Integer, BlockingQueue<Message>>(requestQueueInitSize, 1);
        }
    }

    /**
     * Make connection Initiator
     *
     * @param localPort
     * @param address
     * @param mode
     */
    public Channel(int localPort, InetAddress address, int remotePort, BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, MODE mode) {
        this(localPort, parser, processor, mode);
        this.address = address;
        this.remotePort = remotePort;
    }

    public abstract void start();

    /**
     * Set init size
     * @param initSize
     * @return
     */
    public Channel setRequestQueueInitSize(int initSize) {
        requestQueueInitSize = initSize;
        return this;
    }

    public InetAddress getAddress() {
        return address;
    }
    public int getRemotePort() {
        return remotePort;
    }

    public int getLocalPort() {
        return localPort;
    }

    public void setSocket(SockT socket) {
        this.socket = socket;
    }

    abstract void setRequestsQueue(ConcurrentHashMap requestsQueue);

    abstract void setReceiver(Receiver receiver);

    abstract void setSender(Sender sender);

    /**
     * For UDP channels
     *
     * @param head
     * @param body
     * @return
     * @throws Exception
     */
    abstract public Message parseMessage(byte[] head, byte[] body);

    abstract public void processMessage(Message message);

    public Message request(Message message, long timeSOT) {

        if (mode != MODE.ALL)
            return null;

        Integer localRequestKey;
        BlockingQueue<Message> blockingQueue;

        // ЭТО ТОЖЕ НЕ ПОМОГАЕТ
        blockingQueue = new ArrayBlockingQueue<Message>(1);

        localRequestKey = message.makeRequestId();

        try {
            //PUT QUEUE INTO MAP SO WE KNOW WE ARE WAITING FOR A RESPONSE
            requestsQueue.put(localRequestKey, blockingQueue);

            if (!sender.send(message)) {
                //WHEN FAILED TO SEND MESSAGE
                requestsQueue.remove(localRequestKey);
                return null;
            }

            Message response;
            try {
                response = blockingQueue.poll(timeSOT, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                return null;
            } catch (Exception e) {
                System.err.println(e);
                return null;
            }

            return response;

        } finally {
            requestsQueue.remove(localRequestKey);
        }

    }

    public boolean send(Message message) {
        if (sender != null && message != null)
            return sender.send(message);

        return false;
    }

    public boolean send(byte[] message) {
        if (sender != null && message != null)
            return sender.send(message);

        return false;
    }

    public boolean send(String message) {
        if (sender != null && message != null)
            return sender.send(message);

        return false;
    }

    public boolean send(JSONObject message) {
        if (sender != null && message != null)
            return sender.send(message);

        return false;
    }

    public boolean send(Serializable object) {
        if (sender != null)
            return sender.send(object);

        return false;
    }

    public Message getLastMessage() {
        return receiver == null ? null : receiver.lastMessage;
    }

    public Message request(Message message) {
        return request(message, 5000);
    }

    @Override
    public String toString() {
        return (address == null ? "->" + localPort : address.getHostName() + ":" + remotePort);
    }
}
