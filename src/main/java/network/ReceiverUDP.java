package network;

import network.messages.Message;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.BlockingQueue;

public class ReceiverUDP extends Receiver {

    private ChannelUDP channel;
    private DatagramSocket socket;

    private byte[] receivingDataBuffer = new byte[64000];
    DatagramPacket inputPacket = new DatagramPacket(receivingDataBuffer, receivingDataBuffer.length);

    ReceiverUDP(ChannelUDP channel, DatagramSocket socket) {
        this.channel = channel;
        this.socket = socket;
        this.setName("Receiver-" + getId() + " for Channel: " + channel.toString());
    }

    @Override
    public void run() {

        try {
            int headLen;
            while (!isInterrupted()) {

                Message message;
                try {
                    socket.receive(inputPacket);

                    byte[] buff = inputPacket.getData();
                    if (buff[Message.TYPE_LENGTH] > 0) {
                        // here requestID
                        headLen = Message.TYPE_LENGTH + 1 + Integer.BYTES;
                    } else {
                        headLen = Message.TYPE_LENGTH + 1;
                    }

                    byte[] head = new byte[headLen];
                    byte[] body = new byte[inputPacket.getLength() - headLen];
                    System.arraycopy(buff, 0, head, 0, headLen);
                    System.arraycopy(buff, headLen, body, 0, inputPacket.getLength() - headLen);

                    message = channel.parseMessage(head, body);

                    if (message == null) {
                        if (interrupted())
                            break;

                        continue;
                    }

                } catch (SocketException e) {
                    if (socket.isClosed())
                        return;
                    //System.err.println(e);
                    continue;
                } catch (IOException e) {
                    //System.err.println(e);
                    continue;
                } catch (Exception e) {
                    //System.err.println(e);
                    continue;
                }

                channel.address = inputPacket.getAddress();
                channel.remotePort = inputPacket.getPort();
                message.setSocketAddress(inputPacket.getSocketAddress());

                if (message.isResponse()) {
                    // это ответ на наш запрос с ID

                    if (!channel.requestsQueue.containsKey(message.getRequestId())) {
                        continue;
                    }

                    // get WAITING POLL
                    BlockingQueue<Message> poll = (BlockingQueue<Message>) channel.requestsQueue.remove(message.getRequestId());
                    // invoke WAITING POLL
                    poll.add(message);
                    continue;
                }

                channel.processMessage(message);
            }

        } finally {
            System.out.println(this + " exit");
        }
    }

    @Override
    public void close() {
        interrupt();
    }


}

