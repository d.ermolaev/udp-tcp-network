package network;

import network.messages.Message;
import org.json.JSONObject;

import java.io.Serializable;

abstract class Sender extends Thread {

    abstract protected boolean send(Message message);

    abstract boolean send(byte[] message);

    abstract boolean send(String message);

    abstract boolean send(JSONObject message);

    abstract boolean send(Serializable message);

    public abstract void close();
}
