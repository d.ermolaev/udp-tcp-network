package network;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Shorts;
import network.messages.*;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class SenderUDP extends Sender {

    protected ChannelUDP channel;
    private DatagramSocket socket;

    byte[] sendingDataBuffer; // = new byte[1024];
    DatagramPacket outPacket;

    SenderUDP(ChannelUDP channel, DatagramSocket socket) {
        this.channel = channel;
        this.socket = socket;
        this.setName("Sender-" + getId() + " for Channel: " + channel.toString());
    }

    @Override
    public void start() {
        // EMPTY!
    }
    @Override
    public void run() {
        // EMPTY!
    }

    @Override
    protected boolean send(Message message) {
        if (channel.address == null || channel.remotePort == 0) {
            return false;
        }

        sendingDataBuffer = toBytes(message);
        if (sendingDataBuffer.length > 64000)
            return false;

        outPacket = new DatagramPacket(sendingDataBuffer, sendingDataBuffer.length, channel.address, channel.remotePort);
        try {
//            if (!channel.socket.isClosed())
            socket.send(outPacket);
        } catch (IOException e) {
            System.err.println("sender:" + e);
            return false;
        }
        return true;
    }

    protected boolean send(byte[] message) {
        return send(new BytesMessage(message));
    }

    protected boolean send(String message) {
        return send(new StringMessage(message));
    }

    protected boolean send(JSONObject message) {
        return send(new JsonMessage(message));
    }

    protected boolean send(Serializable object) {
        return send(new ObjectMessage(object));
    }

    public byte[] toBytes(Message message) {

        if (message.flags > 0) {
            return Bytes.concat(Shorts.toByteArray((short) message.type), new byte[]{message.flags},
                    Ints.toByteArray(message.requestId),
                    message.body);
        } else {
            return Bytes.concat(Shorts.toByteArray((short) message.type), new byte[]{0},
                    message.body);
        }

    }

    @Override
    public void close() {
        interrupt();
    }

}
