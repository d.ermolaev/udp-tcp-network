package network;

import network.messages.Message;

abstract public class Receiver extends Thread {

    public Message lastMessage;

    public abstract void close();

}

