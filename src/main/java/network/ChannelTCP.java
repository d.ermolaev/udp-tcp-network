package network;

import network.messages.Message;
import network.messages.MessageProcessor;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

public class ChannelTCP extends Channel<Socket> {

    public final Network network;

    /**
     * As Acceptor
     *
     * @param network
     * @param socket
     * @param parser
     * @param processor
     * @param mode
     */
    public ChannelTCP(Network network, Socket socket, BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, MODE mode) {
        super(socket.getLocalPort(), parser, processor, mode);
        this.network = network;
        this.socket = socket;
    }

    private ChannelTCP(Network network, int localPort, InetAddress address, int remotePort,
                       BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, MODE mode) throws IOException {
        super(localPort, address, remotePort, parser, processor, mode);
        this.network = network;
        this.socket = new Socket(address, remotePort);
    }

    public static ChannelTCP makeConnectionInitiator(Network network, InetAddress address, int remotePort,
                                                     BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, MODE mode) throws IOException {
        return new ChannelTCP(network, 0, address, remotePort, parser, processor, mode);
    }

    /**
     * Make and start Initiator in Only Send mode
     *
     * @param address
     * @param remotePort
     * @return
     */
    public static ChannelTCP startSender(Network network, InetAddress address, int remotePort) throws IOException {
        ChannelTCP channel = new ChannelTCP(network, 0, address, remotePort, null, null, MODE.ONLY_SEND);
        channel.start();
        return channel;
    }


    @Override
    public void setRequestsQueue(ConcurrentHashMap requestsQueue) {
        this.requestsQueue = requestsQueue;
    }

    @Override
    public void setReceiver(Receiver receiver) {
        this.receiver = (ReceiverTCP) receiver;
    }

    @Override
    public void setSender(Sender sender) {
        this.sender = (SenderTCP) sender;
    }

    @Override
    public Message parseMessage(byte[] head, byte[] body) {
        return parser.apply(head, body);
    }

    @Override
    public void processMessage(Message message) {
        processor.process(message);
    }

    public void close() throws IOException {
        if (socket.isConnected() || !socket.isClosed()) {
            if (receiver != null)
                receiver.close();
            if (sender != null)
                sender.close();
            socket.close();
            // Remove from Active Channels
            network.removeChannel(socket.getInetAddress());
        }
    }

    public void start() {

        if (socket == null)
            return;

        if (mode == Channel.MODE.ALL) {
            requestsQueue = new ConcurrentHashMap<Integer, BlockingQueue<Message>>(requestQueueInitSize, 1);
        }

        if (mode != Channel.MODE.ONLY_SEND) {
            receiver = new ReceiverTCP(this, socket);
            receiver.start();
        }

        if (mode != Channel.MODE.ONLY_RECEIVE) {
            sender = new SenderTCP(this, socket);
            sender.start();
        }

    }

    @Override
    public String toString() {
        return "TCP " + super.toString();
    }
}
