package network;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Shorts;
import network.messages.*;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class SenderTCP extends Sender {

    ChannelTCP channel;

    private Socket socket;
    public OutputStream out;
    private final int QUEUE_LENGTH = 32;
    BlockingQueue<Message> sendQueue = new ArrayBlockingQueue<Message>(QUEUE_LENGTH);

    SenderTCP(ChannelTCP channel, Socket socket) {
        this.channel = channel;
        this.socket = socket;
        try {
            out = socket.getOutputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        this.setName("Sender-" + getId() + " for Channel: " + channel.toString());

    }


    @Override
    public synchronized boolean send(Message message) {
        return sendQueue.offer(message);
    }

    protected boolean send(byte[] message) {
        return send(new BytesMessage(message));
    }

    protected boolean send(String message) {
        return send(new StringMessage(message));
    }

    protected boolean send(JSONObject message) {
        return send(new JsonMessage(message));
    }

    protected boolean send(Serializable object) {
        return send(new ObjectMessage(object));
    }

    public byte[] toBytes(Message message) {

        if (message.flags > 0) {
            return Bytes.concat(Shorts.toByteArray((short) message.type), new byte[]{message.flags},
                    Ints.toByteArray(message.requestId),
                    Ints.toByteArray(message.body.length), message.body);
        } else {
            return Bytes.concat(Shorts.toByteArray((short) message.type), new byte[]{0},
                    Ints.toByteArray(message.body.length), message.body);
        }

    }

    @Override
    public void run() {

        if (out == null)
            return;

        try {
            Message message;

            while (true) {

                try {
                    message = sendQueue.take();
                } catch (InterruptedException e) {
                    //System.out.println(this + " InterruptedException");
                    break;
                }

                if (message == null)
                    continue;

                try {
                    byte[] data = toBytes(message);
                    out.write(data);
                    out.flush();
                } catch (InterruptedIOException e) {
                    //System.out.println(this + " InterruptedException");
                    break;
                } catch (IOException e) {
                    //System.err.println(this + " IO ERROR " + e.getMessage());
                    break;
                } catch (Exception e) {
                    System.err.println(this + " ERROR " + e.getMessage());
                    //break;
                }

            }

        } finally {
            System.out.println(this + " exit");

            try {
                channel.network.closeChannel(channel.socket.getInetAddress());
            } catch (Exception e) {
            }
        }

    }

    @Override
    public void close() {
        interrupt();
    }

}
