package network.messages;

import network.Channel;

abstract public class MessageProcessor {

    protected Channel channel;

    public abstract void process(Message message);

    public void setChannel(Channel chanel) {
        this.channel = chanel;
    }

}
