package network.messages;


import java.io.*;

public class ObjectMessage extends Message {

    private Object object;

    public ObjectMessage(byte[] head, byte[] body) throws Exception {
        super(OBJECT_TYPE, head, body);

        ByteArrayInputStream buffer = new ByteArrayInputStream(body);
        ObjectInputStream deserializer = new ObjectInputStream(buffer);
        object = deserializer.readObject();
    }

    public ObjectMessage(byte[] body) throws Exception {
        super(OBJECT_TYPE, body);

        ByteArrayInputStream buffer = new ByteArrayInputStream(body);
        ObjectInputStream deserializer = new ObjectInputStream(buffer);
        object = deserializer.readObject();
    }

    public ObjectMessage(Serializable object) {

        super(OBJECT_TYPE);
        try (ByteArrayOutputStream buffer = new ByteArrayOutputStream()) {
            try (ObjectOutputStream serializer = new ObjectOutputStream(buffer)) {
                serializer.writeObject(object);
                buffer.flush();
                body = buffer.toByteArray();
            }
        } catch (IOException e) {
            System.err.println("writeObject ERROR: " + e.getMessage());
        }
        this.object = object;
    }

    public Object getObject() {
        return object;
    }

    public String toString() {
        return super.toString() + " | " + object.toString();
    }

}
