package network.messages;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;

public class JsonMessage extends Message {

    private JSONObject json;

    public JsonMessage(byte[] head, byte[] body) throws Exception {
        super(JSON_TYPE, head, body);
        this.json = new JSONObject(new String(body, StandardCharsets.UTF_8));
    }

    public JsonMessage(byte[] body) throws Exception {
        super(JSON_TYPE, body);
        this.json = new JSONObject(new String(body, StandardCharsets.UTF_8));
    }

    public JsonMessage(JSONObject json) {
        super(JSON_TYPE, json.toString().getBytes(StandardCharsets.UTF_8));
        this.json = json;
    }

    public JSONObject getJson() {
        return json;
    }

    public String toString() {
        return super.toString() + " | " + json.toString();
    }

}
