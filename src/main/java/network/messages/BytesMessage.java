package network.messages;

public class BytesMessage extends Message {

    public BytesMessage(byte[] head, byte[] body) {
        super(BYTES_TYPE, head, body);
    }

    public BytesMessage(byte[] body) {
        super(BYTES_TYPE, body);
    }

    public String toString() {
        return super.toString() + " | data";
    }

}
