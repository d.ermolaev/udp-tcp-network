package network.messages;

import java.nio.charset.StandardCharsets;

public class StringMessage extends Message {

    protected String message;

    public StringMessage(byte[] head, byte[] body) {
        super(STRING_TYPE, head, body);
        message = new String(body, StandardCharsets.UTF_8);
    }

    public StringMessage(String message) {
        super(STRING_TYPE, message.getBytes(StandardCharsets.UTF_8));
        this.message = message;

    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return super.toString() + " | " + message;
    }

}
