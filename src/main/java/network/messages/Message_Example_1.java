package network.messages;

import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * Example complex Message
 */
public class Message_Example_1 extends Message {

    private int par1;
    private String par2;

    public Message_Example_1(byte[] head, byte[] body) {
        super(Message.EXAMPLE_1_TYPE, head, body);

        par1 = Ints.fromByteArray(Arrays.copyOfRange(this.body, 0, Integer.BYTES));
        par2 = new String(Arrays.copyOfRange(this.body, Integer.BYTES, body.length), StandardCharsets.UTF_8);

    }

    public Message_Example_1(byte[] body) {
        super(Message.EXAMPLE_1_TYPE, body);

        par1 = Ints.fromByteArray(Arrays.copyOfRange(body, 0, Integer.BYTES));
        par2 = new String(Arrays.copyOfRange(body, Integer.BYTES, body.length), StandardCharsets.UTF_8);

    }

    public Message_Example_1(int par1, String par2) {
        super(EXAMPLE_1_TYPE);
        this.par1 = par1;
        this.par2 = par2;

        body = Bytes.concat(Ints.toByteArray(this.par1), par2.getBytes(StandardCharsets.UTF_8));
    }

    public int getPar1() { return par1; }
    public String getPar2() { return par2; }


    @Override
    public String toString() {
        return getClass().getCanonicalName() + "(" + par1 + ", " + par2 + ")";
    }
}
