package network.messages;

import com.google.common.primitives.Ints;
import com.google.common.primitives.Shorts;

import java.net.SocketAddress;
import java.util.Arrays;

public abstract class Message {

    public static final int TYPE_LENGTH = Short.BYTES;
    public static final int RESPONSE_ID_LENGTH = Integer.BYTES;

    public byte flags;
    public int type;
    /**
     * used for check responses
     */
    public int requestId;

    public byte[] body;

    // for UDP responses and list of connections
    private SocketAddress socketAddress;

    public static final int NONE_TYPE = 0;
    public static final int BYTES_TYPE = 1;
    public static final int STRING_TYPE = 2;
    public static final int JSON_TYPE = 3;
    public static final int OBJECT_TYPE = 4;
    public static final int EXAMPLE_1_TYPE = 11;

    public Message(int type, byte[] head, byte[] body) {
        this.type = type;
        flags = head[Message.TYPE_LENGTH];
        if (flags > 0)
            this.requestId = Ints.fromByteArray(Arrays.copyOfRange(head, TYPE_LENGTH + 1, head.length));

        this.body = body;

    }

    public Message(int type, byte[] body) {
        this.type = type;
        this.body = body;
    }

    public Message(int type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        // TODO release?
        return super.hashCode();
    }

    public byte[] getBody() {
        return body;
    }

    public static String viewType(int type) {
        switch (type) {
            case 14:
                return "TT";
            default:
                return "!!!" + type;
        }
    }

    public String toString() {
        return this.getClass().getSimpleName() + "[" + this.type + "]" + (socketAddress == null? "" : socketAddress.toString())
                + (isRequest() ? " ->[id:" + this.requestId + "]" : (isResponse() ? " <-[id:" + this.requestId + "]" : ""));
    }

    public boolean isRequest() {
        return (flags & 1) != 0;
    }

    public boolean isResponse() {
        return (flags & 2) != 0;
    }

    public boolean hasId() {
        return flags > 0;
    }

    public int makeRequestId() {
        flags = 1;
        return (requestId = super.hashCode());
    }

    public int getRequestId() {
        return requestId;
    }

    public void setResponseId(int requestId) {
        flags = 2;
        this.requestId = requestId;
    }

    public SocketAddress getSocketAddress() {
        return socketAddress;
    }
    public void setSocketAddress(SocketAddress socketAddress) {
        this.socketAddress = socketAddress;
    }
    public int getType() {
        return this.type;
    }

    public String viewType() {
        return viewType(this.type);
    }

    public int getDataLength() {
        return TYPE_LENGTH + 1 + (flags > 0 ? RESPONSE_ID_LENGTH : 0) + body.length;
    }

    public static Message DEFAULT_PARSER(byte[] head, byte[] body) {

        try {

            int type = Shorts.fromByteArray(Arrays.copyOfRange(head, 0, Message.TYPE_LENGTH));

            switch (type) {
                case Message.BYTES_TYPE:
                    return new BytesMessage(head, body);
                case Message.STRING_TYPE:
                    return new StringMessage(head, body);
                case Message.JSON_TYPE:
                    return new JsonMessage(head, body);
                case Message.OBJECT_TYPE:
                    return new ObjectMessage(head, body);
            }
            System.err.println("DEFAULT_PARSER: Wrong type [%type] of Message".replaceAll("%type", "" + type));
        } catch (Exception e) {
            System.err.println("DEFAULT_PARSER:" + e.getMessage());
            throw new RuntimeException(e);
        }

        return null;

    }

    public static MessageProcessor EMPTY_PROCESSOR = new EMPTY_PROCESSOR_1();

    private static class EMPTY_PROCESSOR_1 extends MessageProcessor {

        @Override
        public void process(Message message) {

        }
    }

}