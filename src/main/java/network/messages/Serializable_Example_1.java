package network.messages;

import java.io.Serializable;

/**
 * Не эффективная встроенная сеарилизация
 */
public class Serializable_Example_1 implements Serializable {
    static final long serialVersionUID = -1L;
    int value1;
    int value2;

    public Serializable_Example_1(int value1, int value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public String toString() {
        return this.getClass().getSimpleName() + " " + value1 + ":" + value2;
    }
}
