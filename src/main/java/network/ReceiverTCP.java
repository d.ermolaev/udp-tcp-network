package network;

import network.messages.Message;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

public class ReceiverTCP extends Receiver {

    private ChannelTCP channel;
    private Socket socket;
    DataInputStream inputStream;

    ReceiverTCP(ChannelTCP channel, Socket socket) {
        this.channel = channel;
        this.socket = socket;
        this.setName("Receiver-" + getId() + " for Channel: " + channel.toString());

        try {
            inputStream = new DataInputStream(socket.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {

        if (inputStream == null)
            return;

        try {

            Message message;

            byte buff[];
            while (!isInterrupted()) {

                try {

                    buff = new byte[Message.TYPE_LENGTH + 1];
                    inputStream.readFully(buff, 0, Message.TYPE_LENGTH + 1);
                    byte[] head;
                    if (buff[Message.TYPE_LENGTH] > 0) {
                        // has requestID
                        head = new byte[Message.TYPE_LENGTH + 1 + Integer.BYTES];
                        System.arraycopy(buff, 0, head, 0, Message.TYPE_LENGTH + 1);
                        inputStream.readFully(head, Message.TYPE_LENGTH + 1, Integer.BYTES);
                    } else {
                        head = buff;
                    }

                    int bodyLen = inputStream.readInt();
                    byte[] body = new byte[bodyLen];
                    inputStream.readFully(body, 0, bodyLen);

                    message = channel.parseMessage(head, body);

                    if (message == null) {
                        if (isInterrupted()) {
                            System.out.println(this + " isInterrupted");
                            break;
                        }

                        // CHECK EOF and IO
                        if (inputStream.available() < 0) {
                            System.out.println(this + " available = -1");
                            break;
                        }

                        // It is an error on parsing - ignore
                        continue;
                    }

                } catch (IOException e) {
                    System.out.println(this + " IOException: " + e.getStackTrace());
                    break;

                } catch (Exception e) {
                    if (isInterrupted()) {
                        System.out.println(this + " isInterrupted 2");
                        break;
                    }

                    // IF catch error in Parse
                    System.err.println(this + " ERROR: " + e.getStackTrace());
                    continue;
                }

                if (message.isResponse()) {
                    // это ответ на наш запрос с ID

                    if (!channel.requestsQueue.containsKey(message.getRequestId())) {
                        continue;
                    }

                    // get WAITING POLL
                    BlockingQueue<Message> poll = channel.requestsQueue.remove(message.getRequestId());
                    // invoke WAITING POLL
                    poll.add(message);
                    continue;
                }

                channel.processMessage(message);

            }

        } finally {
            System.out.println(this + " exit");

            try {
                channel.network.closeChannel(channel.socket.getInetAddress());
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void close() {
        interrupt();
    }

}

