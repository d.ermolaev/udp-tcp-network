package network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionAcceptorTCP extends Thread {

    public int MAX_CONNECTIONS = 10;

    public final Network network;
    private ServerSocket serverSocket;

    public ConnectionAcceptorTCP(Network network) {
        this.network = network;
    }

    public void run() {

        while (!isInterrupted()) {

            // protect from Initiators spam
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                break;
            }

            //CHECK IF WE HAVE MAX CONNECTIONS
            if (network.getActiveCovers().size() > MAX_CONNECTIONS) {
                continue;
            }

            try {

                if (serverSocket == null || serverSocket.isClosed())
                    serverSocket = new ServerSocket(network.port);

                // WAIT FOR ACCEPT CONNECTION
                Socket connectionSocket = serverSocket.accept();
                String remoteAddress = connectionSocket.getInetAddress().getHostAddress();

                //CHECK IF SOCKET IS NOT LOCALHOST || WE ARE ALREADY CONNECTED TO THAT SOCKET || BLACKLISTED
                if (network.getBannedIP().contains(remoteAddress)
                        || network.getActiveCovers().containsKey(remoteAddress)
                ) {
                    //DO NOT CONNECT TO OURSELF/EXISTING CONNECTION
                    // or BANNED
                    connectionSocket.shutdownOutput();
                    connectionSocket.close();
                    continue;
                }

                //CREATE CHANNEL
                ChannelTCP chanel = new ChannelTCP(network, connectionSocket, network.parser, network.processor, network.mode);
                network.addChannel(connectionSocket.getInetAddress(), chanel);
                chanel.start();

            } catch (java.net.SocketException e) {

                if (interrupted())
                    break;

                System.err.println(this + " " + e.getMessage());

                if (!serverSocket.isClosed())
                    try {
                        serverSocket.close();
                    } catch (Exception e1) {
                    }

                serverSocket = null;

            } catch (Exception e) {
                System.err.println(this + " " + e.getMessage());

                try {
                    if (serverSocket != null) serverSocket.close();
                } catch (Exception e1) {
                }

            }

        }

        if (serverSocket != null && !serverSocket.isClosed())
            try {
                serverSocket.close();
            } catch (Exception ex) {
            }

        System.out.println(this + " exited");
    }

    public void close() {
        if (serverSocket == null)
            return;

        interrupt();

        try {
            serverSocket.close();
        } catch (IOException e) {
        }
        try {
            join();
        } catch (Exception e) {
        }

    }

    @Override
    public String toString() {
        return "ConnectionAcceptorTCP ->" + network.port;
    }

}
