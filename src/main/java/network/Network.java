package network;

import network.messages.Message;
import network.messages.MessageProcessor;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.BiFunction;

public class Network implements AutoCloseable {

    public final int port;
    public final Channel.MODE mode;

    public final BiFunction<byte[], byte[], Message> parser;
    public final MessageProcessor processor;

    ConnectionAcceptorTCP connectionAcceptor;

    private HashMap<InetAddress, ChannelTCP> activeCovers = new HashMap<>();
    private HashSet<String> bannedIP = new HashSet<>();

    public Network(int localPort, BiFunction<byte[], byte[], Message> parser, MessageProcessor processor, Channel.MODE mode) {
        this.port = localPort;

        this.parser = parser;
        this.processor = processor;

        this.mode = mode;
    }

    public void makeAcceptor() {
        connectionAcceptor = new ConnectionAcceptorTCP(this);
    }

    public void start() {
        connectionAcceptor.start();
    }

    public void addChannel(InetAddress address, ChannelTCP channel) {
        activeCovers.put(address, channel);
    }

    public HashMap<InetAddress, ChannelTCP> getActiveCovers() {
        return activeCovers;
    }

    public HashSet<String> getBannedIP() {
        return bannedIP;
    }

    public synchronized void closeChannel(InetAddress address) {
        ChannelTCP channel = activeCovers.remove(address);
        if (channel != null) {
            try {
                channel.close();
            } catch (IOException e) {
            }
        }
    }
    public synchronized void removeChannel(InetAddress address) {
        activeCovers.remove(address);
    }

    public void close() {
        connectionAcceptor.close();

        for (ChannelTCP peer : activeCovers.values()) {
            try {
                peer.close();
            } catch (Exception e) {
            }
        }
    }

}
