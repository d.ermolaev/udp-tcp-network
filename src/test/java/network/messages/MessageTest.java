package network.messages;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

class MessageTest {

    /**
     * Эффективность преобразования. Встроенный сериализатор Явы не эффективный!
     * зато ручная сериализация дала бы всего 8 байт. Если лень ее делать - JSON
     */
    @Test
    void testLen() {
        Serializable_Example_1 myObj = new Serializable_Example_1(23, 87968);
        JSONObject json = new JSONObject();
        json.put("value1", myObj.value1);
        json.put("value2", myObj.value2);
        JsonMessage messJson = new JsonMessage(json);
        ObjectMessage messObj = new ObjectMessage(myObj);
        System.out.println("OBJ -> Serialize -> Bytes len: " + messObj.body.length);
        System.out.println("OBJ -> JSON -> Str -> Bytes len: " + messJson.body.length);
        System.out.println("OBJ -> hand made toBytes & parse -> Bytes len: " + (Integer.BYTES + Integer.BYTES));
    }

    @Test
    void isRequest() {
    }
}