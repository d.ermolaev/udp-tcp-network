package network;

import network.messages.BytesMessage;
import network.messages.Message;
import network.messages.MessageProcessor;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

class ChannelTCPAcceptorTest {

    int CONNECTION_PORT = 10111;
    InetAddress acceptorIP;

    {
        try {
            acceptorIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    Channel.MODE mode = Channel.MODE.ALL;

    @Test
    public void asAcceptor() {

        Network network = new Network(CONNECTION_PORT, Message::DEFAULT_PARSER,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        byte[] data = ((BytesMessage) message).getBody();
                        System.out.println(data.length + "\n" + new String(data, 8));
                    }
                },
                mode);

        network.makeAcceptor();
        network.start();
        System.out.println("Acceptor TCP is start on port $CONNECTION_PORT_TCP");
        System.out.println("Use tests for check examples");

    }


}