package network;

import network.messages.Message;
import network.messages.MessageProcessor;
import network.messages.StringMessage;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.function.BiFunction;

class RequestUDPTest {

    int CONNECTION_PORT = 10101;
    InetAddress acceptorIP;
    {
        try {
            acceptorIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    Channel.MODE mode = Channel.MODE.ALL;

    /**
     * test Requests to channel
     */
    @Test
    void requestUDP() {

        final BiFunction<byte[], byte[], Message> parser = Message::DEFAULT_PARSER;

        try (ChannelUDP channel = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
                parser,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {

                    }
                },
                mode)) {
            channel.start();

            Message requestMessage = new StringMessage("Hello");
            System.out.println("WAITING RESPONSE...");
            Message responseMessage = channel.request(requestMessage, 200);
            if (responseMessage == null)
                System.out.println("null");
            else
                System.out.println(((StringMessage) responseMessage).getMessage());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


    // TODO request unique ID
    @Test
    void messHashCode() {
        StringMessage mess = new StringMessage("saad");
        int hh = mess.makeRequestId();
        int hh2 = mess.hashCode();
        StringMessage mess1 = new StringMessage("saad");
        Object mess3 = new Object();
        mess3.hashCode();

    }

}