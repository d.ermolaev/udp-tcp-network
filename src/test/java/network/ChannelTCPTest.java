package network;

import com.google.common.primitives.Shorts;
import network.messages.Message;
import network.messages.MessageProcessor;
import network.messages.Message_Example_1;
import network.messages.StringMessage;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.function.BiFunction;

class ChannelTCPTest {

    int CONNECTION_PORT = 10101;
    InetAddress acceptorIP;

    {
        try {
            acceptorIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    Channel.MODE mode = Channel.MODE.ALL;

    @Test
    void sendAsChannelInitiatorTCP() {

        final BiFunction<byte[], byte[], Message> parserTCP = (head, body) -> {
            try {
                int type = Shorts.fromByteArray(Arrays.copyOfRange(head, 0, Message.TYPE_LENGTH));
                byte flags = head[Message.TYPE_LENGTH];

                switch (type) {
                    case Message.EXAMPLE_1_TYPE:
                        // Parse EXAMPLE_1_TYPE message
                        return flags > 0 ? new Message_Example_1(head, body)
                                : new Message_Example_1(body);
                    default:
                        return Message.DEFAULT_PARSER(head, body);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };

        Network network = new Network(CONNECTION_PORT, parserTCP,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode);

        try (ChannelTCP channelTCP = ChannelTCP.makeConnectionInitiator(network, acceptorIP,
                network.port, network.parser, network.processor, network.mode)) {

            channelTCP.start();

            Message_Example_1 mess1 = new Message_Example_1(123, "Hello");
            channelTCP.send(mess1);

            synchronized (mess1) {
                mess1.wait(20);
            }

            Message_Example_1 mess2 = new Message_Example_1(3, "World!");
            channelTCP.send(mess2);

            synchronized (mess1) {
                mess1.wait(20);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    void requestTCP() {

        final BiFunction<byte[], byte[], Message> parserTCP = (head, body) -> {
            try {
                int type = Shorts.fromByteArray(Arrays.copyOfRange(head, 0, Message.TYPE_LENGTH));
                byte flags = head[Message.TYPE_LENGTH];

                switch (type) {
                    case Message.EXAMPLE_1_TYPE:
                        // Parse EXAMPLE_1_TYPE message
                        return flags > 0 ? new Message_Example_1(head, body)
                                : new Message_Example_1(body);
                    default:
                        return Message.DEFAULT_PARSER(head, body);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };

        Network network = new Network(CONNECTION_PORT, parserTCP,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode);

        try (ChannelTCP channelTCP = ChannelTCP.makeConnectionInitiator(network, acceptorIP,
                network.port, network.parser, network.processor, network.mode)) {

            channelTCP.start();

            Message requestMessage = new StringMessage("Request Hello");
            System.out.println("WAITING RESPONSE...");
            Message responseMessage = channelTCP.request(requestMessage, 2000);
            if (responseMessage == null)
                System.out.println("null");
            else
                System.out.println(((StringMessage) responseMessage).getMessage());

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    public void sendAsChannelInitiatorTCP_2() {

        final BiFunction<byte[], byte[], Message> parserTCP = (head, body) -> {
            try {
                int type = Shorts.fromByteArray(Arrays.copyOfRange(head, 0, Message.TYPE_LENGTH));
                byte flags = head[Message.TYPE_LENGTH];

                switch (type) {
                    case Message.EXAMPLE_1_TYPE:
                        // Parse EXAMPLE_1_TYPE message
                        return flags > 0 ? new Message_Example_1(head, body)
                                : new Message_Example_1(body);
                    default:
                        return Message.DEFAULT_PARSER(head, body);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };

        Network network = new Network(CONNECTION_PORT, parserTCP,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode);

        try (ChannelTCP channelTCP = ChannelTCP.makeConnectionInitiator(network, acceptorIP,
                network.port, network.parser, network.processor, network.mode)) {

            channelTCP.start();


            channelTCP.send("mess1".getBytes(StandardCharsets.UTF_8));
            channelTCP.send("mess1");
            JSONObject json = new JSONObject();
            json.put("topic", "MIR");
            json.put("name", "DMITRY");
            channelTCP.send(json);

            synchronized (channelTCP) {
                channelTCP.wait(100);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


}