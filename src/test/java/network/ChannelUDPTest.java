package network;

import com.google.common.primitives.Shorts;
import jdk.nashorn.internal.ir.annotations.Ignore;
import network.messages.*;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.function.BiFunction;

import static org.junit.jupiter.api.Assertions.fail;

class ChannelUDPTest {

    int CONNECTION_PORT_DEFAULT_PARSER = 10100;
    int CONNECTION_PORT = 10101;
    InetAddress acceptorIP;

    {
        try {
            acceptorIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    Channel.MODE mode = Channel.MODE.ALL;

    /**
     * Test default PARSER
     */
    @Test
    public void sendAsChannelInitiatorUDP_Default_Parses() {

        final BiFunction<byte[], byte[], Message> parser = Message::DEFAULT_PARSER;

        try (ChannelUDP channel = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT_DEFAULT_PARSER,
                parser,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode)) {
            channel.start();

            Message mess1 = new StringMessage("Hello 2");
            System.out.println("try send: " + mess1);
            channel.send(mess1);

            Message mess2 = new Message_Example_1(123, "Hello");
            System.out.println("try send: " + mess2);
            channel.send(mess2);

            // wait for backward send from Acceptor
            synchronized (mess1) {
                mess1.wait(20);
            }
        } catch (Exception e) {
        }

        // NEW CHANNEL
        try (ChannelUDP channel = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
                parser,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode)) {
            channel.start();

            Message mess1 = new StringMessage("Hello 2");
            System.out.println("try send: " + mess1);
            channel.send(mess1);

            Message mess2 = new Message_Example_1(123, "Hello");
            System.out.println("try send: " + mess2);
            channel.send(mess2);

            // wait for backward send from Acceptor
            synchronized (mess1) {
                mess1.wait(20);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * Serializable
     */
    @Test
    public void sendAsChannelInitiatorUDP_Serializable() {

        try (ChannelUDP channelSender = ChannelUDP.startSender(acceptorIP, CONNECTION_PORT_DEFAULT_PARSER)) {

            Serializable_Example_1 mess1 = new Serializable_Example_1(1, 3);
            System.out.println("try send OBJECT: " + mess1);
            channelSender.send(mess1);

            // wait for backward send from Acceptor
            synchronized (mess1) {
                mess1.wait(50);
            }
        } catch (Exception e) {
        }

    }


    /**
     * run Main Func first for start Channel Acceptor before this tests
     */
    @Test
    public void sendAsChannelInitiatorUDP() {

        final BiFunction<byte[], byte[], Message> parser = (head, body) -> {
            try {
                int type = Shorts.fromByteArray(Arrays.copyOfRange(head, 0, Message.TYPE_LENGTH));
                byte flags = head[2];

                switch (type) {
                    case Message.EXAMPLE_1_TYPE:
                        // Parse EXAMPLE_1_TYPE message
                        return flags > 0 ? new Message_Example_1(head, body)
                                : new Message_Example_1(body);
                    default:
                        return Message.DEFAULT_PARSER(head, body);
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };

        try (ChannelUDP channel = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
                parser,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode)) {
            channel.start();

            Message mess1 = new Message_Example_1(123, "Hello");
            System.out.println("try send: " + mess1);
            channel.send(mess1);

            Message_Example_1 mess2 = new Message_Example_1(3, "World!");
            System.out.println("try send: " + mess2);
            channel.send(mess2);

            // wait for backward send from Acceptor
            synchronized (mess1) {
                mess1.wait(20);
            }
        } catch (Exception e) {
        }

        // NEW CHANNEL
        try (ChannelUDP channel = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
                parser,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode)) {
            channel.start();

            Message mess1 = new StringMessage("Hello 2");
            System.out.println("try send: " + mess1);
            channel.send(mess1);

            // wait for backward send from Acceptor
            synchronized (mess1) {
                mess1.wait(20);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Test
    public void sendAsChannelInitiatorUDP_2() {

        final BiFunction<byte[], byte[], Message> parser = (head, body) -> {
            try {
                int type = Shorts.fromByteArray(Arrays.copyOfRange(head, 0, Message.TYPE_LENGTH));
                byte flags = head[2];

                switch (type) {
                    case Message.EXAMPLE_1_TYPE:
                        // Parse EXAMPLE_1_TYPE message
                        return flags > 0 ? new Message_Example_1(head, body)
                                : new Message_Example_1(body);
                    default:
                        return Message.DEFAULT_PARSER(head, body);
                }

            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };

        try (ChannelUDP channel = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
                parser,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                mode)) {
            channel.start();

            channel.send("mess1".getBytes(StandardCharsets.UTF_8));
            channel.send("mess1");
            JSONObject json = new JSONObject();
            json.put("topic", "MIR");
            json.put("name", "DMITRY");
            channel.send(json);

            synchronized (channel) {
                channel.wait(100);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        ChannelUDP channel = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
                parser, Message.EMPTY_PROCESSOR,
                mode);
    }

    class messageProcessorMe extends MessageProcessor {
        @Override
        public void process(Message message) {
            // some processing
            System.out.println(message.toString());
        }
    }

    /**
     * Тут должно при работающем Main вылететь ошибка - Address already in use: Cannot bind
     */
    @Ignore
    @Test
    public void sendAsChannelAcceptorUDP() {

        // or use Message.EMPTY_PROCESSOR
        ChannelUDP channelAcceptor = ChannelUDP.makeConnectionAcceptor(CONNECTION_PORT,
                Message::DEFAULT_PARSER, new messageProcessorMe(),
                Channel.MODE.ALL);

        channelAcceptor.start();

        // тут обработка входящих сообщений и исходящих - пока программа работает

        // Initiator канала
        final BiFunction<byte[], byte[], Message> parser = Message::DEFAULT_PARSER;
        ChannelUDP channelInitiator = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
                parser, new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        System.out.println(message);
                    }
                },
                Channel.MODE.ALL);

        channelInitiator.start();

        Message mess1 = new StringMessage("Hello 2");
        System.out.println("try send: " + mess1);
        channelAcceptor.send(mess1);

        Message mess2 = new Message_Example_1(123, "Hello");
        System.out.println("try send: " + mess2);
        channelInitiator.send(mess2);

        Message response = channelInitiator.request(mess2, 2000);
        System.out.println("Response: " + response);

        // wait for backward send from Acceptor
        synchronized (mess1) {
            try {
                mess1.wait(200);
            } catch (InterruptedException e) {
            }
        }

    }

    @Test
    public void multiSend() {


        try (ChannelUDP channel1 = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT_DEFAULT_PARSER,
                Message::DEFAULT_PARSER,
                new MessageProcessor() {
                    @Override
                    public void process(Message message) {
                        // simple show received message
                        String mess = ((StringMessage) message).getMessage();
                        if (!mess.startsWith("channel_1:")) {
                            System.out.println("ERROR:" + message);
                            fail("ERROR:" + message);
                        }

                    }
                },
                Channel.MODE.ALL)) {
            try (ChannelUDP channel2 = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT_DEFAULT_PARSER,
                    Message::DEFAULT_PARSER,
                    new MessageProcessor() {
                        @Override
                        public void process(Message message) {
                            // simple show received message
                            String mess = ((StringMessage) message).getMessage();
                            if (!mess.startsWith("channel_2:")) {
                                System.out.println("ERROR:" + message);
                                fail("ERROR:" + message);
                            }

                        }
                    },
                    Channel.MODE.ALL)) {

                try (ChannelUDP channel3 = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT_DEFAULT_PARSER,
                        Message::DEFAULT_PARSER,
                        new MessageProcessor() {
                            @Override
                            public void process(Message message) {
                                // simple show received message
                                String mess = ((StringMessage) message).getMessage();
                                if (!mess.startsWith("channel_3:")) {
                                    System.out.println("ERROR:" + message);
                                    fail("ERROR:" + message);
                                }

                            }
                        },
                        Channel.MODE.ALL)) {

                    channel1.start();
                    channel2.start();
                    channel3.start();

                    Runnable task1 = new Runnable() {
                        public void run() {
                            int i = 0;
                            Message resp;
                            while (true) {
                                resp = channel1.request(new StringMessage("channel_1 " + i++));
                                System.out.println("1 <- " + resp);
                                try {
                                    Thread.sleep(10);
                                } catch (InterruptedException e) {
                                    break;
                                }
                            }
                        }
                    };
                    Thread thread1 = new Thread(task1);;

                    Runnable task2 = new Runnable() {
                        public void run() {
                            int i = 0;
                            Message resp;
                            while (true) {
                                resp = channel2.request(new StringMessage("channel_2 " + i++));
                                System.out.println("2 <- " + resp);
                                try {
                                    Thread.sleep(11);
                                } catch (InterruptedException e) {
                                    break;
                                }
                            }
                        }
                    };
                    Thread thread2 = new Thread(task2);

                    Runnable task3 = new Runnable() {
                        public void run() {
                            int i = 0;
                            Message resp;
                            while (true) {
                                resp = channel3.request(new StringMessage("channel_3 " + i++));
                                System.out.println("3 <- " + resp);
                                try {
                                    Thread.sleep(12);
                                } catch (InterruptedException e) {
                                    break;
                                }
                            }
                        }
                    };
                    Thread thread3 = new Thread(task3);

                    thread1.start();
                    thread2.start();
                    thread3.start();

                    Boolean www = Boolean.TRUE;
                    // wait for backward send from Acceptor
                    synchronized (www) {
                        www.wait(1000);
                    }
                } catch (Exception e) {
                }
            } catch (Exception e) {
            }
        } catch (Exception e) {
        }

    }

}