package network

import com.google.common.primitives.Ints
import network.Channel.MODE
import network.messages.Message
import network.messages.MessageProcessor
import network.messages.Message_Example_1
import network.messages.StringMessage
import org.junit.jupiter.api.Test
import java.net.InetAddress
import java.util.*

internal class ChannelUDPTest2 {

    val CONNECTION_PORT = 10101
    var acceptorIP: InetAddress? = InetAddress.getByName("localhost")
    var mode = MODE.ALL

    /**
     * run Main Func first for start Channel Acceptor before this tests
     */
    @Test
    fun sendAsClient() {
        val parser =
            Function@{ head: ByteArray, body: ByteArray ->
                val type = Ints.fromByteArray(
                    Arrays.copyOfRange(
                        head,
                        0,
                        Message.TYPE_LENGTH
                    )
                )
                when (type) {
                    Message.EXAMPLE_1_TYPE ->                         // Parse EXAMPLE_1_TYPE message
                        return@Function Message_Example_1(head, body)

                    else -> return@Function Message.DEFAULT_PARSER(head, body)
                }
            }

        ChannelUDP.makeConnectionInitiator(
            acceptorIP, CONNECTION_PORT,
            parser,
            object : MessageProcessor() {
                override fun process(message: Message) {
                    // simple show received message
                    println(message)
                }
            },
            mode
        ).use { channel ->
            channel.start()
            val mess1: Message = Message_Example_1(123, "Hello")
            println("try send: $mess1")
            channel.send(mess1)
            val mess2 = Message_Example_1(3, "World!")
            println("try send: $mess2")
            channel.send(mess2)

        }

        // NEW CHANNEL
        ChannelUDP.makeConnectionInitiator(
            acceptorIP, CONNECTION_PORT,
            parser,
            object : MessageProcessor() {
                override fun process(message: Message) {
                    // simple show received message
                    println(message)
                }
            },
            mode
        ).use { channel ->
            channel.start()
            val mess1: Message = StringMessage("Hello 2")
            println("try send: $mess1")
            channel.send(mess1)

            // wait for backward send from Acceptor
            ///synchronized(mess1) { mess1.wait(20) }
        }

    }

}