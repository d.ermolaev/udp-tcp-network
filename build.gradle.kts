plugins {
    kotlin("jvm") version "1.8.0"
    application
}

group = "ru.icreator"
version = "1.4"

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.google.guava", "guava", "31.0-jre")

    implementation("org.json:json:20220924")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

application {
    mainClass.set("MainKt")
}

sourceSets {
    main {
        java {
            srcDir("src/main/java")
        }
        kotlin {
            srcDir("src/main/kotlin")
        }
    }

    test {
        java {
            srcDir("src/test/java")
        }
        kotlin {
            srcDir("src/test/kotlin")
        }
    }

}
