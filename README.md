# UDP-TCP-Network

Используйте этот пакет для создания канала обмена данными через соединение по протоколам UDP или TCP между разными
проектами, сервисами, серверами или устройствами. Вдобавок канал может обрабатывать "запросы" (request), которые
блокируют выполнение кода на определенное время до получения ответа с другой стороны Канала (аналог получений страницы
сайта по URL).

## UDP channels

На уровне UDP-протокола каналы не существуют, поэтому любой может послать данные на заданный порт и адрес. Логический
канал (network.**ChannelUDP**) создается Приемником соединения (Acceptor - метод network.ChannelUDP#*
*makeConnectionAcceptor**) на одной стороне, и Зачинателем соединения (Initiator - метод network.ChannelUDP#*
*makeConnectionInitiator**) на другой. Сперва запускается программа (служба, сервер), в которой выполнен Приемник
соединения, затем программа где выполнен Зачинатель. Приемник прослушивает заданный порт и при входе запроса от
Зачинателя создает соединение на своей стороне, а Зачинатель - на своей.
Для описания канала нужно задать два обработчика - Сборщик сообщений из байт, полученных из канала (парcер) - *
*parseMessage**, и Решатель сообщений - **processMessage**.
В качестве Сборщика сообщений есть стандартный, который обрабатывает либо байтовые, либо строковые либо JSON-сообщения и
запросы: network.messages.Message#**DEFAULT_PARSER_UDP**.

## TCP channels

Эти каналы устроены сложнее, при получении входящего запроса создается устойчивое соединение на уровне протокола,
которое имеет состояния работы (закрыто, отключено). Поэтому принимающая сторона организует список открытых каналов, для
чего создан класс Network, который и запускает непрерывный обработчик входящих запросов на создание канала: network.*
*ConnectionAcceptorTCP**.
При разрыве канала, он удаляется из списка активных каналов, так чтобы можно было опять на его место переподключиться.
Так же есть возможность ввести список забанненых адресов.

Класс Network задает порт, на котором будет создано множество входящих соединений. Вообще каждый узел сети (отдельный
компьютер) может запустить на своей стороне и как и Приемник запросов на соединение так и Зачинателей - так например
организуется одноранговая сеть в блокчейн. Данная библиотека вполне для этого подходит

## Функционал

+ Send (.send) - послать сообщение
+ Process (.processMessage) - обработать полученное сообщение с помощью заданного Решателя
+ Request (.request) - послать запрос и ждать ответа

## Направления Канала

При создании Канала можно использовать описание направлений - MODE:
- [x] ALL - в обоих направления, и послать и принять можно,
- [x] ONLY_RECEIVE - только принимать, поток отправки не запускается и запросы не работают,
- [x] ONLY_SEND - только послать, поток приемки не запускается и запросы не работают.

## Установка

Создайте JAR файл и используйте его в проектах на Maven вставив в файл pom.xml:

```xml
<dependency>
    <groupId>ru.icreator</groupId>
    <artifactId>udp-tcp</artifactId>
    <version>1.4</version>
    <scope>system</scope>
    <systemPath>${project.basedir}/../udp-tcp-network/build/libs/udp-tcp-network-1.4.jar</systemPath>
</dependency>
```
Номер версии последний подставить. Возможно нужно поставить дополнительные ../ в пути - зависит от местоположения проекта

Так же в pom.xml нужно вставить зависимости:
```xml
<dependency>
    <groupId>org.json</groupId>
    <artifactId>json</artifactId>
    <version>20220320</version>
</dependency>
<!-- иначе вылетает ошибка: java.lang.NoClassDefFoundError: com/google/common/primitives/Ints -->
<dependency>
    <groupId>com.google.guava</groupId>
    <artifactId>guava</artifactId>
    <version>31.1-jre</version>
</dependency>
```

И исключить встроенный JSON из Springboot:
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-test</artifactId>
    <scope>test</scope>
    <!-- XML to JSON, see https://stackoverflow.com/questions/52371365/nosuchmethod-error-when-using-org-json-to-convert-xml-to-json -->
    <exclusions>
        <exclusion>
            <groupId>com.vaadin.external.google</groupId>
            <artifactId>android-json</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

## Examples
Код запуск приемщика соединений UDP:
```java
        ChannelUDP channelAcceptor = ChannelUDP.makeConnectionAcceptor(CONNECTION_PORT,
        Message::DEFAULT_PARSER, new messageProcessorMe(),
        Channel.MODE.ALL);

        channelAcceptor.start();

```
Где `MessageUDP::DEFAULT_PARSER` - стандартная процедура для парсинга сообщений. Вместо нее можно вставить свою. 

А класс для обработки принятых сообщений `messageProcessorMe` выглядит так:
```java
    class messageProcessorMe extends MessageProcessor {
        @Override
        public void process(Message message) {
            // some processing
            System.out.println(message.toString());
        }
    }

```
В нем нужно задать метод `process`.

Код для запуска инициатора сообщений, например, будет такой:
```java
        final BiFunction<byte[], byte[], Message> parser = Message::DEFAULT_PARSER;
        ChannelUDP channelInitiator = ChannelUDP.makeConnectionInitiator(acceptorIP, CONNECTION_PORT,
        parser, new MessageProcessor() {
                @Override
                public void process(Message message){
        // simple show received message
        System.out.println(message);
        }
        },
        Channel.MODE.ALL);

        channelInitiator.start();

```

Если нужно создать канал только для посылок, без чтения и запросов, то можно использовать упрощенный вызов для его
создания и старта:

```java
    udpChannel=ChannelUDP.startSender(InetAddress.getByName(Host),port);
```

Далее послать сообщение или запрос в канал можно так:

```java
        Message mess1 = new StringMessage("Hello 2");
        System.out.println("try send: "+mess1);
        channelAcceptor.send(mess1);

        Message mess2 = new Message_Example_1(123,"Hello");
        System.out.println("try send: "+mess2);
        channelInitiator.send(mess2);

        Message response = channelInitiator.request(mess2, 2000);
        System.out.println("Response: " + response);

```

Для запуска примеров, запустите основное приложение (MainKt#main). После чего в модуле Tests можете запускать разные
тесты и смотреть результаты.

> Written with [StackEdit](https://stackedit.io/).